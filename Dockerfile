FROM node:18-alpine
COPY dist /app
WORKDIR /app
COPY node_modules node_modules
COPY package.json .
ENTRYPOINT node index.js