import type { Models } from "appwrite";

export const CART_COLLECTION = "633168d505af161da4cb";
export type CartItem = {commodityId:string,quantity:number,addedBy:string} & Models.Document;