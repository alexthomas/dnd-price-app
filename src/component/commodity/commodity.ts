import type { Models } from "appwrite";

export type Commodity={
    name?: string;
    description?: string;
    photo?: string;
    storeType?: Array<string>;
    regionType?: Array<string>;
    path?: Array<string>;
    cost?: number;
    limitedStock?: boolean;
    createdDateTime: number;
    updatedDateTime: number;
    imageUrl:string;
    previewUrl: string;
    iconUrl: string;
} & Models.Document;
export const COMMODITIES_BUCKET = "633165714f2ae044e3fd";
export const COMMODITIES_COLLECTION = "633167fe19162c6d1ef5";
