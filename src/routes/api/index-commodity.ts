import {client as enterpriseSearchClient} from '@service/api/enterprise-search-client';
import {client as redisClient} from '@service/api/redis-client';
import type {Commodity as c} from "@component/commodity/commodity";

type Commodity = c & {id:string};

export async function post({request}: any) {
    const data:Commodity = await request.json();
    data.id = data.$id;
    await storeTree(data);
    return Promise.all([indexCommodity(data), storeTree(data), cacheCommodity(data)]);
}

async function indexCommodity(commodity:Commodity){
    const indexObject: { [key: string]: any } = {};
    for (const key in commodity) {
        if (key.indexOf("$") === -1) {
            // @ts-ignore
            indexObject[key.replace(/([A-Z])/g, "_$1").toLowerCase()] = commodity[key];
        }
    }
    const response = await enterpriseSearchClient.app.indexDocuments({engine_name: "dpa", documents: [indexObject]});
    return {body: response};
}

async function storeTree(commodity:Commodity){
    const multi = redisClient.multi();
    for (let storeType of commodity.storeType||[]) {
        multi.sAdd("store|"+storeType.toUpperCase()+"|commodities",commodity.id);
        multi.sAdd("store|root|children",storeType.toUpperCase());
    }
    if(commodity.path && commodity.path[0]){
        multi.sAdd("path|root|children",commodity.path[0])
        multi.sAdd(`path|${commodity.path[commodity.path.length-1]}|commodities`,commodity.id)
        for (let i = 1; i < commodity.path.length; i++) {
            multi.sAdd(`path|${commodity.path[i-1]}|children`,commodity.path[i])
        }
    }

    await multi.exec();
}

async function cacheCommodity(commodity:Commodity){
    await redisClient.set("commodity|"+commodity.id,JSON.stringify(commodity));
}