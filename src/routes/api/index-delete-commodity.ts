import {client} from '@service/api/enterprise-search-client';

export async function post({request}: any) {
    const data = await request.json();
    return client.app.deleteDocuments({engine_name:'dpa',documentIds:[data.$id]});
}