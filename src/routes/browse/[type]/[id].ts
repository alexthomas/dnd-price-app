import {client} from '@service/api/redis-client'

/** @type {import('./__types/[id]').RequestHandler}*/
export async function get({params: {type, id}}: { params: { type: string, id: string } }) {
    const childrenIds = await client.sMembers(`${type}|${id}|children`);
    const commodityIds = await client.sMembers(`${type}|${id}|commodities`);
    const children = await Promise.all(childrenIds.map(cid=>getChild(type,cid)));
    const commodities = await Promise.all(commodityIds.map(cid => client.get(`commodity|` + cid).then(s => s ? JSON.parse(s) : undefined)));
    return  {body:{children, commodities}};
}

async function getChild(type:string,id:string):Promise<{id:string,commodityCount:number,childCount:number}>{
   const commodityCount = await client.sCard(`${type}|${id}|commodities`);
   const childCount = await client.sCard(`${type}|${id}|children`);
   return {id,commodityCount,childCount};
}