import {createClient} from "redis";

export const client = createClient({url:`redis://${process.env.REDIS_HOST||'localhost'}:6379`});
client.on('error',err=>console.error("Redis error",err));

await client.connect();

