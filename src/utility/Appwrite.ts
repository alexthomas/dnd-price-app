import {Account, Avatars, Client, Databases,Storage} from 'appwrite';

export const client = new Client();
export const ENDPOINT = "https://appwrite.alexk8s.com/v1";
export const PROJECT = "632674ff62c6fafa9612";
export const DATABASE = "633167f7611b3e6782d6";
client.setEndpoint(ENDPOINT)
    .setProject(PROJECT);
const databases = new Databases(client);
const account = new Account(client);
const avatars = new Avatars(client);
const storage = new Storage(client);

export const appwrite = {databases,account,avatars,storage,client}