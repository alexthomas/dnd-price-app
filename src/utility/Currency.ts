
export function formatCurrency(amount: number,format:'copper'|'silver'|'gold'|'platinum'|'auto'):string{
    switch (format){
        case "copper":
            return formatValue(amount*100)+"cp";
        case "silver":
            return formatValue(amount*10)+"sp";
        case "gold":
            return formatValue(amount)+"gp";
        case "platinum":
            return formatValue(amount/10)+"pp";
    }

    if (amount % 10 === 0) {
        return `${amount / 10}pp`;
    }
    if (amount < .1) {
        return `${formatValue(amount * 100)}cp`;
    } else if (amount < 1) {
        return `${formatValue(amount * 10)}sp`;
    }
    return `${formatValue(amount)}gp`;
}

function formatValue(value:number):string{
    if(value%1>.01){
        return value.toFixed(2);
    }
    return value.toFixed(0);
}