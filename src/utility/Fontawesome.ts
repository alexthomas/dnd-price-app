export {faImageSlash} from "@fortawesome/pro-solid-svg-icons/faImageSlash";
export {faAlicorn} from '@fortawesome/pro-regular-svg-icons/faAlicorn';
export {faExternalLink} from '@fortawesome/pro-regular-svg-icons/faExternalLink';
export {faCoin} from '@fortawesome/pro-regular-svg-icons/faCoin';
export {faCoins} from '@fortawesome/pro-regular-svg-icons/faCoins';
export {faPencil} from '@fortawesome/pro-solid-svg-icons/faPencil';
export {faTrash} from '@fortawesome/pro-solid-svg-icons/faTrash';
export {faSackDollar} from '@fortawesome/pro-solid-svg-icons/faSackDollar';
export {faCartShopping} from '@fortawesome/pro-solid-svg-icons/faCartShopping';
export {faCartCirclePlus} from '@fortawesome/pro-solid-svg-icons/faCartCirclePlus';
export {faSpaceStationMoonAlt} from '@fortawesome/pro-solid-svg-icons/faSpaceStationMoonAlt';
export {faUserHelmetSafety} from '@fortawesome/pro-solid-svg-icons/faUserHelmetSafety';
export {default as Fa} from "svelte-fa/src/fa.svelte";

