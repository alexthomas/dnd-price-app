import {account as accountStore} from "@utility/Stores";
import {Permission, Role} from "appwrite";
let account:any;
accountStore.subscribe((a)=>account=a);
export function canEdit(record:any){
    const userKey = Permission.update(Role.user(account?.$id));
    return record.$permissions.indexOf(userKey)!==-1;
}
