import {writable} from "svelte/store";
import type {Models} from "appwrite";

type Preferences = { currency: 'copper' | 'silver' | 'gold' | 'platinum' | 'auto' }
export const account = writable<Models.User<Preferences>>();
export const header = writable("DnD Price App");
