import adapter from '@sveltejs/adapter-node';
import preprocess from 'svelte-preprocess';
import {resolve} from "path";
/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess({postcss:true}),

	kit: {
		adapter: adapter({out:'dist'}),
		files:{
			assets: './static'
		},
		vite:{
			ssr:{
				noExternal:['@elastic/search-ui']
			},
			resolve:{
				alias:{
					"@component": resolve("./src/component"),
					"@utility": resolve("./src/utility"),
					"@service": resolve("./src/service")
				}
			}
		}
	}
};

export default config;
